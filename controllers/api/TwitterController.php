<?php
/**
 * Created by PhpStorm.
 * User: savki
 * Date: 19.05.2018
 * Time: 19:28
 */

namespace app\controllers\api;

use Yii;
use yii\web\Controller;

class TwitterController extends Controller
{

    private $passId = 'WBYX1TLPRWJ7NSV36LCPP2OZFH6AE6LM';
    private $filename = __DIR__.'/names.php';

    public function actionAdd($id = null, $user = null, $secret = null){

        if ($result = $this->validateParams($id, $user, $secret)) {
            return json_encode($result);
        }

        $names = $this->getArrayData();
        if(!in_array($user, $names)){
            $names[] = $user;
        }
        if($this->setArrayData($names)) {
            return '';
        }

    }

    public function actionFeed($id = null, $secret = null){
        if ($result = $this->validateParams($id, '', $secret)) {
            return json_encode($result);
        }
        $names = $this->getArrayData();
        if(!$names) {
            return '';
        }
        $tweets['feed'] = [];
        foreach ($names as $name) {
            $twitter = Yii::$app->twitter->getTwitter();

            $result = $twitter->get('statuses/user_timeline', [
                'screen_name' => $name,
            ]);

            if(!$result->error){
                if(!$result->errors) {
                    $tweets['feed'][] = [
                        'name' =>$name,
                        'tweet' => count($result) > 0 ? $result[0]->text : '',
                        'hashtag' => count($result) > 0 ? $result[0]->hashtags : [],
                    ];
                } else {
                    $tweets['feed'][] = [
                        'name' =>$name,
                        'tweet' => $result->errors[0]->message,
                        'hashtag' => []
                    ];
                }
            } else {
                $tweets['feed'][] = [
                    'name' =>$name,
                    'tweet' => $result->error,
                    'hashtag' => []
                ];
            }
        }
        return json_encode($tweets);
    }

    public function actionRemove($id = null, $user = null, $secret = null){
        if ($result = $this->validateParams($id, $user, $secret)) {
            return json_encode($result);
        }

        $names = $this->getArrayData();
        if(!$names) {
            return '';
        }
        if(in_array($user, $names)){
            $this->del_from_array($user, $names);
        }

        if($this->setArrayData($names)) {
            return '';
        }
    }

    private function access($id, $user, $secret)
    {
        if( ($id != $this->passId) || sha1($id.$user) != $secret){
            return false;
        }
        return true;
    }

    private function validateParams($id, $user, $secret)
    {
        if($user === null) {
            return ["error" => "missing parameter"];
        }

        if(!$this->access($id, $user, $secret)) {
            return ["error" => "access denied"];
        }

        if($user){
            $twitter = Yii::$app->twitter->getTwitter();
            $searchUser = $twitter->get('users/lookup', [
                'screen_name' => $user,
            ]);
            if($searchUser->errors) {
                return ["error" => "no such user"];
            }
        }

    }

    private function getArrayData()
    {
        if(!file_exists($this->filename)){
            return [];
        }
        return unserialize(file_get_contents($this->filename));
    }

    private function setArrayData($data)
    {
        return file_put_contents($this->filename, serialize($data));
    }

    public function del_from_array($needle, &$array, $all = true){
        if(!$all){
            if(FALSE !== $key = array_search($needle,$array)) unset($array[$key]);
            return;
        }
        foreach(array_keys($array,$needle) as $key){
            unset($array[$key]);
        }
    }
}